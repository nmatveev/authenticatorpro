# FROM mcr.microsoft.com/dotnet/sdk:${DOTNET_VERSION} AS dotnet-maui-android
# FROM debian:bookworm-slim AS dotnet-maui-android
FROM debian:bullseye-slim AS dotnet-maui-android

# ARG DOTNET_VERSION="7.0"
# ARG DOTNET_SDK_VERSION="7.0.404"
ARG DOTNET_SDK_VERSION="7.0"
# NET 7.0.404 dotnet_sha512='f5c122044e9a107968af1a534051e28242f45307c3db760fbb4f3a003d92d8ea5a856ad4c4e8e4b88a3b6a825fe5e3c9e596c9d2cfa0eca8d5d9ee2c5dad0053'
ARG MAINTAINER
ARG TZ="UTC"

LABEL maintainer=${MAINTAINER}
ENV TZ=${TZ}
ENV DOTNET_CLI_TELEMETRY_OPTOUT=1

# NET SDK
ENV \
    # Unset ASPNETCORE_URLS from aspnet base image
    ASPNETCORE_URLS= \
    # Do not generate certificate
    DOTNET_GENERATE_ASPNET_CERTIFICATE=false \
    # Do not show first run text
    DOTNET_NOLOGO=true \
    # SDK version
    # DOTNET_SDK_VERSION=${DOTNET_SDK_VERSION} \
    # Enable correct mode for dotnet watch (only mode supported in a container)
    DOTNET_USE_POLLING_FILE_WATCHER=true \
    # Skip extraction of XML docs - generally not useful within an image/container - helps performance
    NUGET_XMLDOC_MODE=skip

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        git \
        wget \
    && rm -rf /var/lib/apt/lists/*

# Install .NET SDK
# RUN curl -fSL --output dotnet.tar.gz https://dotnetcli.azureedge.net/dotnet/Sdk/${DOTNET_SDK_VERSION}/dotnet-sdk-${DOTNET_SDK_VERSION}-linux-x64.tar.gz \
#     # && dotnet_sha512='f5c122044e9a107968af1a534051e28242f45307c3db760fbb4f3a003d92d8ea5a856ad4c4e8e4b88a3b6a825fe5e3c9e596c9d2cfa0eca8d5d9ee2c5dad0053' \
#     # && echo "$dotnet_sha512  dotnet.tar.gz" | sha512sum -c - \
#     && mkdir -p /usr/share/dotnet \
#     && tar -oxzf dotnet.tar.gz -C /usr/share/dotnet ./packs ./sdk ./sdk-manifests ./templates ./LICENSE.txt ./ThirdPartyNotices.txt \
#     && rm dotnet.tar.gz \
#     # Trigger first run experience by running arbitrary cmd
#     && dotnet help
RUN <<EOF
    set -e
    wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    dpkg -i packages-microsoft-prod.deb
    rm packages-microsoft-prod.deb
    apt-get update
    apt-get install -y dotnet-sdk-${DOTNET_SDK_VERSION}
    rm -rf /var/lib/apt/lists/*
EOF

# JAVA SDK
ARG JAVA_VERSION="11"
RUN <<EOF
  set -e
  echo "deb http://deb.debian.org/debian bullseye-backports main contrib" > /etc/apt/sources.list.d/bullseye-backports.list
  echo "deb-src http://deb.debian.org/debian bullseye-backports main contrib" >> /etc/apt/sources.list.d/bullseye-backports.list
  apt-get update
  apt-get install -t bullseye-backports -y openjdk-${JAVA_VERSION}-jdk-headless
  rm -rf /var/lib/apt/lists/*
EOF

ENV JAVA_HOME=/usr/lib/jvm/java-${JAVA_VERSION}-openjdk-amd64/

# Android SDK Manager
RUN <<EOF
  set -e
  echo "deb http://deb.debian.org/debian bullseye-backports main contrib" > /etc/apt/sources.list.d/bullseye-backports.list
  echo "deb-src http://deb.debian.org/debian bullseye-backports main contrib" >> /etc/apt/sources.list.d/bullseye-backports.list
  apt-get update
  apt-get install -t bullseye-backports -y sdkmanager
  rm -rf /var/lib/apt/lists/*
EOF

ENV ANDROID_SDK_ROOT=/usr/lib/android-sdk

# Android toolchain
ARG ANDROID_API="33"
ARG BUILD_TOOLS_VERSION="33.0.2"
RUN <<EOF
  set -e
  sdkmanager "platform-tools" "build-tools;${BUILD_TOOLS_VERSION}" "platforms;android-${ANDROID_API}"
EOF

ARG MAUI_VERSION
# MAUI (We can only install the latest version, the ARG is to cache bust if that version changed)
RUN <<EOF
  set -e
  dotnet workload install maui-android
EOF
